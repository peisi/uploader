<?php
define('REALM', 'HcUp');
define('DEBUG', TRUE);
define('USER', 'test');
define('PASS', 'test');
define('DOWNLOAD_DIR', '/tmp/'); //tailing '/' is required.
define('APPEND', '.hcup');
define('ALLOWED_EXTENSION', array('txt'));

if(DEBUG){
  error_reporting(E_ALL);
}
set_time_limit(0);

$q = strtolower(trim($_SERVER['QUERY_STRING']));
if(strlen($q)==40){
  if($file=getFile($q)){
    header('Content-type:application/octet-stream');
    $filename = substr($file, 41, -strlen(APPEND));

    header('Content-Disposition: attachment; filename="'.$filename.'"');
    readfile(DOWNLOAD_DIR.$file);
    exit;
  }
}

session_start();
$message = '';
if (isset($_SESSION['login'])){
    $login = TRUE;
}else{
  if(isset($_POST['username']) && isset($_POST['password']) && $_POST['username']===USER && $_POST['password']===PASS){
    $_SESSION['login']=TRUE;
    $login = TRUE;
  }else{
    $login = FALSE;
  }
}

if($login){
  processUp();
}

//unfunction in cgi environment.
/*
if (!isset($_SERVER['HTTP_AUTHORIZATION'])) {
  header('WWW-Authenticate: Basic realm="'.REALM.'d"');
  header('HTTP/1.0 401 Unauthorized');
  echo 'Canceled login.';
  print_r($_SERVER);
  print_r($_ENV);

  exit;
} elseif (preg_match('/Basic\s+(.*)$/i', $_SERVER['HTTP_AUTHORIZATION'], $matches)){
  list($name, $password) = explode(':', base64_decode($matches[1]));
  $_SERVER['PHP_AUTH_USER'] = strip_tags($name);
  $_SERVER['PHP_AUTH_PW'] = strip_tags($password);
  if($_SERVER['PHP_AUTH_PW'] != PASS || $_SERVER['PHP_AUTH_USER'] != USER){
    header('WWW-Authenticate: Basic realm="'.REALM.'df"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Auth failed.';

    exit;
  }
}else{
  exit;
}
 */


function processUp(){
  global $message;
  if(!empty($_FILES['userfile']['name'])){

    //TODO: Sanitize the filename
    $filename = basename($_FILES['userfile']['name']);
    $hash = sha1($filename);

    $uploadfile = DOWNLOAD_DIR . sha1($filename).'.'.$filename. APPEND;
    if(getFile($hash)){
      $uploadfile.='.r';
      $replace = TRUE;
    }

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
      if(isset($replace)){
        alert('The same filename aleady exists');
        $message .= fileDetail($hash,$filename,$uploadfile);
        $message .= '<form method="POST"><input name="hash" type="hidden" value="'.$hash.'"><input type="submit" name="submit" value="Replace"> <input type="submit" name="submit" value="Cancel"></form>';
      }else{
        alert('Upload success.');
        $message .= fileDetail($hash, $filename, $uploadfile);
      }
    }else {
      alert('Error','err');
      if(DEBUG){
        $message .= '<pre>'. print_r($_FILES, true) . '</pre>';
      }
    }
  }

  //File replace operations.
  if(isset($_POST['hash'])){
    $hash = strtolower(trim($_POST['hash']));
    if($file = getFile($hash)){
      switch($_POST['submit']){
      case 'Replace':
        if(rename(DOWNLOAD_DIR.$file.'.r', DOWNLOAD_DIR.$file)){
          alert('Replace success.');
          $message .= fileDetail($hash,NULL,$file);
        }else{
          alert('Replace failed.');
        }
        break;
      case 'Cancel':
        if(@unlink(DOWNLOAD_DIR.$file.'.r')){
          alert('Replace canceled.');
          $message .= fileDetail($hash,NULL,$file);
        }
        break;
      default:
        break;
      }
    }
  }
}

function fileDetail($hash, $filename=NULL, $uploadfile=NULL){
  $message = '';
  if(!isset($uploadfile)){
    $uploadfile = getFile($hash);
  }
  if(!isset($filename)){
    $filename = substr($uploadfile, 41, -strlen(APPEND));
  }
  $message .= '<a href="?'.$hash.'">'.$filename.'</a>';
  return $message;
}

function alert($msg, $level = 'msg'){
  global $message;
  $message .= '<p class="'.$level.'">'.$msg.'</p>';
}

function getFile($hash){
  $hash = strtolower(trim($hash));
  if($dh = opendir(DOWNLOAD_DIR)){
    while (($entry = readdir($dh))!=FALSE){
      if(substr($entry,0,40) == $hash && substr($entry,-strlen(APPEND)) == APPEND){
        closedir($dh);
        return $entry;
      }
    }
    closedir($dh);
  }
  return FALSE;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo REALM;?></title>
  </head>
  <body>
<?php if($login):?>
<fieldset>
<legend><span><?php echo REALM;?></span></legend>
<form enctype="multipart/form-data" method="POST" >
    <!-- MAX_FILE_SIZE must precede the file input field -->
    <input type="hidden" name="MAX_FILE_SIZE" value="500000000" />
    <!-- Name of input element determines name in $_FILES array -->
    Send this file: <input name="userfile" type="file" />
    <input type="submit" value="Send File" />
</form>
</fieldset>
<?php else: ?>
<form method="POST">
<input type="text" name="username" >
<input type="password" name="password">
<input type="submit" value="Login">
</form>

<?php endif;?>

<?php echo $message;?>
  </body>
</html>
